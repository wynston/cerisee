'use strict';

import React from 'react';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Button } from 'react-bootstrap';

require('styles//Navbar.css');

class NavbarComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    clearLogin () {
        localStorage.setItem('userId', false);
        localStorage.setItem('accessToken', false);
        localStorage.setItem('user', false);
    }
    render() {
        let token = localStorage.getItem('accessToken')|| this.props.accessToken;
        let user = JSON.parse(localStorage.getItem('user')) || this.props.user;
        let userInfo;
        if (user && token){
            let username = user.username || user.email;
             userInfo =  (
                 <Navbar.Text pullRight>
                    <Navbar.Link href="/user">{username}</Navbar.Link>
                    <Navbar.Link href={'/service/logout?accessToken='+token} onClick={this.clearLogin}>Logout</Navbar.Link>
                 </Navbar.Text>
             ); 
        } else {
            userInfo =  (
             <Navbar.Text pullRight>
                <Navbar.Link href="/login">Login</Navbar.Link>
                <Navbar.Link href="/register">Register</Navbar.Link>
             </Navbar.Text>
         );
      }
    return (
      <div className="navbar-component">
        <Navbar collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="/">车厘子</a>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <NavItem eventKey={1} href="#"></NavItem>
                <NavItem eventKey={2} href="#">Link</NavItem>
                <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                  <MenuItem eventKey={3.1}>Action</MenuItem>
                  <MenuItem eventKey={3.2}>Another action</MenuItem>
                  <MenuItem eventKey={3.3}>Something else here</MenuItem>
                  <MenuItem divider />
                  <MenuItem eventKey={3.3}>Separated link</MenuItem>
                </NavDropdown>
              </Nav>
                
                {userInfo}
                
            </Navbar.Collapse>
          </Navbar>
      </div>
    );
  }
}

NavbarComponent.displayName = 'NavbarComponent';

// Uncomment properties you need
// NavbarComponent.propTypes = {};
// NavbarComponent.defaultProps = {};

export default NavbarComponent;
