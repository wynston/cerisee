'use strict';

import React from 'react';
import axios from 'axios';

import { Form, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import NavbarComp from './NavbarComponent';
import Footer from './FooterComponent';

import {browserHistory} from "react-router";

require('styles//Register.css');

class RegisterComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            username: '',
            password: '',
            register: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        let obj = this.state;
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }

    handleSubmit(event) {
        event.preventDefault();
        axios.post('/service/api/users', {
            email: this.state.email,
            username: this.state.username,
            password: this.state.password
        })
        .then(function (response) {
            console.log(response);
            if(response.data.success) {
                this.setState({
                    register: true
                });
            }
            /*
            this.setState({
                result:response.data.results
            });
            console.log();
            */
        }.bind(this))
        .catch(function (error) {
            console.log(error);
        }.bind(this));

    }
  render() {
      let html = (
          <Form onSubmit={this.handleSubmit}>
            <FormGroup controlId="formInlineName">
              <ControlLabel>Name</ControlLabel>
              <FormControl name="username" type="text" placeholder="Jane Doe" value={this.state.username} onChange={this.handleChange}/>
            </FormGroup>
            <FormGroup controlId="formInlineEmail">
              <ControlLabel>Email</ControlLabel>
              <FormControl name="email" type="email" placeholder="jane.doe@example.com" value={this.state.email} onChange={this.handleChange}/>
            </FormGroup>
            <FormGroup controlId="formInlinePassword">
              <ControlLabel>Password</ControlLabel>
              <FormControl name="password" type="password" placeholder="" value={this.state.password} onChange={this.handleChange} />
            </FormGroup>
            <Button type="submit">
              Register
            </Button>
            <Button bsStyle="link" href="/login">
              Login
            </Button>
          </Form>
    )
      if(this.state.register) {
          html = (
            <div>
            <h4>Register Success!</h4>
            <br/>
            <p>Please check your email and click on the verification link before logging in.</p>
            <a href="/login">Login</a>
              </div>
          )
      }
    return (
      <div className="register-component">
        <NavbarComp/>
        
        <div className="container">
          {html}
        </div>
        <Footer/>
      </div>
    );
  }
}

RegisterComponent.displayName = 'RegisterComponent';

// Uncomment properties you need
// RegisterComponent.propTypes = {};
// RegisterComponent.defaultProps = {};

export default RegisterComponent;
