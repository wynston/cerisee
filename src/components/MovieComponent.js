'use strict';

import React from 'react';
import axios from 'axios'

require('styles//Movie.css');

class MovieComponent extends React.Component {
    constructor(props) {
      super(props);
        
      this.state = {
        value: props.query,
          result: []
      };
    axios.get('/service/movie/info/'+ props.match.params.id)
      .then(function (response) {
        this.setState({result:response.data})
        console.log();
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      }.bind(this))
        
    }
  render() {
     const item = this.state.result;
    return (
      <div className="movie-component">
        <img src={item.poster_path? ('https://image.tmdb.org/t/p/w500/' + item.poster_path):'/assets/498c110c39325a46e36ff16b9ce11583.png'} />
        <h2>片名:{item.title}</h2>
        <h5>上映时间：{item.release_date}</h5>
        <h5>评分：{item.vote_average}</h5>
        <p>简介：{item.overview}</p>
      </div>
    );
  }
}

MovieComponent.displayName = 'MovieComponent';

// Uncomment properties you need
// MovieComponent.propTypes = {};
// MovieComponent.defaultProps = {};

export default MovieComponent;
