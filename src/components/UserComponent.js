'use strict';

import React from 'react';
import NavbarComp from './NavbarComponent';
import Footer from './FooterComponent';
import axios from 'axios';


require('styles//User.css');

class UserComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: '',
            accessToken: ''
        };
        let accessToken = localStorage.getItem('accessToken');
        let userId = localStorage.getItem('userId');
        if( userId && accessToken) {
            axios.get('/service/api/users/' + userId, {
                params: {
                    access_token: accessToken
                }
            }).then(function(response) {
                console.log(response.data);
                this.setState({
                    user: response.data
                })
                localStorage.setItem('user', JSON.stringify(response.data));
                this.setState({
                    accessToken: accessToken,
                    user: response.data
                })
            }.bind(this))
        } else {
            location.href = '/login'
        }
        
    }
  render() {
    return (
      <div className="user-component">
        <NavbarComp user={this.state.user} accessToken={this.state.accessToken} />
        <div className="container">
            {this.state.user.email}
            {this.state.user.username}
        </div>
        <Footer/>
      </div>
    );
  }
}

UserComponent.displayName = 'UserComponent';

// Uncomment properties you need
// UserComponent.propTypes = {};
// UserComponent.defaultProps = {};

export default UserComponent;
