'use strict';

import React from 'react';
import NavbarComp from './NavbarComponent';
import Footer from './FooterComponent';

require('styles//Verified.css');

class VerifiedComponent extends React.Component {
  render() {
    return (
      <div className="verified-component">
        <NavbarComp />
        <div className="container">
            <h1>Registration verified successfully</h1>
            You are now ready to <a href="/login">log in</a>.
        </div>
        <Footer />
      </div>
    );
  }
}

VerifiedComponent.displayName = 'VerifiedComponent';

// Uncomment properties you need
// VerifiedComponent.propTypes = {};
// VerifiedComponent.defaultProps = {};

export default VerifiedComponent;
