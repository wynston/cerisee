require('video.js/dist/video-js.css')
require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';

import Movie from './MovieComponent';
import Home from './HomeComponent';
import Register from './RegisterComponent';
import RegisterSuccess from './RegisterSuccessComponent';
import Login from './LoginComponent';
import ResetPassword from './ResetPasswordComponent';
import Verified from './VerifiedComponent';
import UserComponent from './UserComponent';
import { BrowserRouter as Router, Route, hashHistory, Link } from 'react-router-dom';








class AppComponent extends React.Component {
  render() {
    
    return (
     <Router history={hashHistory}>
        <div>
            <Route path="/"  exact component={Home}/>
            <Route path="/search/:query" component={Home}/>
            <Route path="/movie/:id" component={Movie}/>
            <Route path="/register" component={Register}/>
            <Route path="/registersuccess" component={RegisterSuccess}/>
            <Route path="/login" component={Login}/>
            <Route path="/verified" component={Verified}/>
            <Route path="/resetpassword" component={ResetPassword}/>
            <Route path="/user" component={UserComponent}/>
        </div>
      </Router>
    )
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
