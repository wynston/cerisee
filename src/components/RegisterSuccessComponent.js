'use strict';

import React from 'react';
import NavbarComp from './NavbarComponent';
import Footer from './FooterComponent';

require('styles//RegisterSuccess.css');

class RegisterSuccessComponent extends React.Component {
  render() {
    return (
      <div className="registersuccess-component">
        <NavbarComp/>
        <div className="container">
            <h4>Register Success!</h4>
            <br/>
            <p>Please check your email and click on the verification link before logging in.</p>
            <a href="/login">Login</a>
        </div>
        <Footer/>
      </div>
    );
  }
}

RegisterSuccessComponent.displayName = 'RegisterSuccessComponent';

// Uncomment properties you need
// RegisterSuccessComponent.propTypes = {};
// RegisterSuccessComponent.defaultProps = {};

export default RegisterSuccessComponent;
