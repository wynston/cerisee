'use strict';

import React from 'react';
import axios from 'axios';

import { Form, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import NavbarComp from './NavbarComponent';
import Footer from './FooterComponent';

require('styles//Login.css');

class LoginComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            accessToken: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        let obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }

    handleSubmit(event) {
        event.preventDefault();
        axios.post('/service/login', {
            username: this.state.username,
            password: this.state.password
        })
        .then(function (response) {
            console.log(response);
            if(response.data.success) {
                console.log(response.data)
                localStorage.setItem('accessToken', response.data.accessToken);
                localStorage.setItem('userId', response.data.userId);
                this.setState({
                    accessToken: response.data.accessToken,
                    userId: response.data.userId
                });
                location.href="/user";
            }
        }.bind(this))
        .catch(function (error) {
            console.log(error);
        }.bind(this));

    }

    render() {
        return (
          <div className="login-component">
            <NavbarComp accessToken={this.state.accessToken}/>
            <div className="container">
                <Form onSubmit={this.handleSubmit}>
                <FormGroup controlId="formInlineName">
                  <ControlLabel>Name or Email</ControlLabel>
                  <FormControl name="username" type="text" placeholder="User Name or Email" value={this.state.username} onChange={this.handleChange}/>
                </FormGroup>
                <FormGroup controlId="formInlinePassword">
                  <ControlLabel>Password</ControlLabel>
                  <FormControl name="password" type="password" placeholder="" value={this.state.password} onChange={this.handleChange} />
                </FormGroup>
                <Button type="submit">
                  Login
                </Button>
                <Button bsStyle="link" href="/register">
                  Register
                </Button>
                </Form>
            </div>
            <Footer/>
          </div>
        );
    }
}

LoginComponent.displayName = 'LoginComponent';

// Uncomment properties you need
// LoginComponent.propTypes = {};
// LoginComponent.defaultProps = {};

export default LoginComponent;
