'use strict';

import React from 'react';

require('styles//ResetPassword.css');

class ResetPasswordComponent extends React.Component {
  render() {
    return (
      <div className="resetpassword-component">
        Please edit src/components///ResetPasswordComponent.js to update this component!
      </div>
    );
  }
}

ResetPasswordComponent.displayName = 'ResetPasswordComponent';

// Uncomment properties you need
// ResetPasswordComponent.propTypes = {};
// ResetPasswordComponent.defaultProps = {};

export default ResetPasswordComponent;
