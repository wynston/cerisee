'use strict';

import React from 'react';
import axios from 'axios';
import {FormGroup, FormControl, HelpBlock, ControlLabel, InputGroup, Button, Col, Thumbnail} from 'react-bootstrap';
import { BrowserRouter as Router, Route, hashHistory, Link } from 'react-router-dom';
import NavbarComponent from './NavbarComponent';
import FooterComponent from './FooterComponent';
require('styles//Home.css');


let yeomanImage = require('../images/yeoman.png');

class FormExample extends React.Component {
    constructor(props) {
      super(props);
        
      this.state = {
        value: props.query||'',
          result: []
      };
    axios.get('/service/movie/search/'+ props.query)
      .then(function (response) {
        this.setState({result:response.data.results})
        console.log();
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      }.bind(this))
        
    }

  getValidationState = ()=>  {
    const length = this.state.value.length;
    //if (length > 10) return 'success';
    //else if (length > 5) return 'warning';
    //else if (length > 0) return 'error';
  }

  handleChange = (e)=>  {
    this.setState({ value: e.target.value });
  }
  componentWillMount = () => {
     
  }

  render() {
     
    return (
      <form>
        <FormGroup
          controlId="formBasicText"
          validationState={this.getValidationState()}
        >
          <ControlLabel>车厘子影视搜索</ControlLabel>
          <InputGroup>
            
            <FormControl
            type="text"
            value={this.state.value}
            placeholder="请输入影片名称"
            onChange={this.handleChange}
          />
            <InputGroup.Button>
            <Button><a href={'/search/' + this.state.value}>搜索</a></Button>
            </InputGroup.Button>
          <FormControl.Feedback />
          </InputGroup>
                                      
          <HelpBlock></HelpBlock>
        </FormGroup>
        <div className = "result"><NumberList numbers={this.state.result} /></div>
      </form>
    );
  }
}

function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((item) =>
    <Col key={item.id} xs={6} md={3} ><a href={'/movie/'+item.id}><Thumbnail src={item.poster_path? ('https://image.tmdb.org/t/p/w500/' + item.poster_path):'/assets/498c110c39325a46e36ff16b9ce11583.png'}>{item.title}</Thumbnail></a></Col>
  );
  return (
    <div className="row">{listItems}</div>
  );
}



class HomeComponent extends React.Component {
  render() {
    return (
      <div className="home-component">
        <NavbarComponent />
        <div className="index container">
            <img src={yeomanImage} alt="Yeoman Generator" />
            <FormExample query={this.props.match.params.query}/>
        
        </div>
        <FooterComponent />
      </div>
    );
  }
}

HomeComponent.displayName = 'HomeComponent';

// Uncomment properties you need
// HomeComponent.propTypes = {};
// HomeComponent.defaultProps = {};

export default HomeComponent;
